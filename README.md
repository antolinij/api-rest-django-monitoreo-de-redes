# API Rest Django - Monitoreo de procesos Linux #

Api desarrollada en Python a través del Framework Django, usando el modulo Api Rest Framework.

### Que encontraremos en este documento. ###

El README se organiza en dos partes.

+ 1- Pasos para su correcta instalación en el servidor Linux.
+ 2- Comandos de prueba GET, POST, DELETE, PUT

### 1- Puesta en marcha del servidor ###

`$ mkdir /home/data/monitoreo`

`$ cd /home/data/monitoreo/`

`$ virtualenv venv` (apt-get install virtualenv)

`$ git clone https://antolinij@bitbucket.org/antolinij/api-rest-django-monitoreo-de-redes.git`

`$ source venv/bin/activate`

`$ pip install -r requirements.txt`

Para el funcionamiento debemos crear usuarios en nuestro virtual env de Python, estos tienen dos roles:

Para esto, ingresamos a nuestra shell del proyecto Django:

`$ python manage.py shell`

+ Administradores:
Con permisos GET, POST, DELETE, PUT

>`$ from django.contrib.auth.models import User `

>`$ User.objects.create_superuser(username='ringo', password='pass', email='ringo_starr@gmail.com') `

>`$ quit() `

+ usuarios comunes con permisos GET solamente

>  `$ from django.contrib.auth.models import User `

> `$ user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword') `

> `$ user.save() `

> `$ quit() `

NOTA: Queda a consideración del administrador de redes, que usuario puede ser super user (root) y cual no.

### 2- Comandos de prueba - uso de la API ###


### Obtención del token de autenticación ###

Con los datos de los usuarios creados antes, realizamos una peticion a la siguiente url:

`$ curl -X POST -d "username=USER&password=PASSUSER" http://HOST:PUERTO/api/token/ `

Este comando nos devuelve un JSON, con el siguiente formato:

`$ {"token":"un token alfanumérico que nos va a servir para autenticarnos"} `

### Metodo GET ###

Apto para usuarios comunes y administradores

> Obtener todos los procesos corriendo en el servidor

>`curl http://HOST:PUERTO/api/process/ -H 'Authorization: Token asdf1234' `

> Obtener todos un proceso en particular a través de su pid (por ej: nro_pid == 2)

>`curl http://HOST:PUERTO/api/process/nro_pid/ -H 'Authorization: Token asdf1234' `


### Metodo POST ###

Apto para administradores

> Ejecutar un proceso nuevo

>`curl -X POST -d "command=ping www.google.com" http://HOST:PUERTO/api/process/ -H 'Authorization: Token asdf1234'`

NOTA: para lanzar un nuevo proceso es necesario indicarlo a través del formato -d "commando= cmd arg1 arg2 arg3 ..." cada parámetro separado por un espacio.

### Metodo PUT ###

Apto para administradores

> Cambiar la prioridad de un proceso, a través del pid enviado en la url

>`curl -X PUT -d "priority=5" http://HOST:PUERTO/api/process/ -H 'Authorization: Token asdf1234'`

NOTA: la prioridad pasada con el formato -d "priority=nro" debe ser un entero comprendido entre -20 y 20.

### Metodo DELETE ###

Apto para administradores

> Eliminar un proceso a través de su pid.

>`curl -X DELETE http://HOST:PUERTO/api/process/nro_pid/ -H 'Authorization: Token asdf1234'`

NOTA: el numero de pid ingresado en la url deberá existir.

### Aclaración para todos los métodos ###

Todos los comandos devuelven un Json con información del comando en particular, por ejemplo el método GET devuelve un JSON con cada clave valor de los procesos, en cambio el método POST devuelve un Json indicando que pudo lanzarse el proceso y se le asigno el pid nro o indicando que no pudo ejecutarse el comando.

### Como contribuir ###

+ Incorporacion SSL a las url.

### Contacto ###

+ jony.antolini@gmail.com