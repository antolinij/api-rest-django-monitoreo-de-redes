from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAdminUser

#imports para el manejo procesos
import psutil, datetime, subprocess, shlex, sh

class ProcessList(APIView):

        authentication_classes= (TokenAuthentication,)
        permission_classes= (IsAuthenticated,)

	def get_permissions(self):
                # Your logic should be all here
                if self.request.method == 'GET':
                        self.permission_classes = [IsAuthenticated, ]
                else:
                        self.permission_classes = [IsAdminUser, ]

                return super(ProcessList, self).get_permissions()

        def get(self, request, format=None):

		process_list= []
                for proc in psutil.process_iter():
                        try:
                       	        pinfo = proc.as_dict(['name', 'pid', 'username', 'cpu_percent', 'memory_percent', 'memory_info', 'terminal', 'status', 'cpu_times', 'cmdline'])
                               	create_time= datetime.datetime.fromtimestamp(proc.create_time()).strftime("%Y-%m-%d %H:%M:%S")
                               	pinfo['create_time']= create_time
                               	process_list.append(pinfo)
                       	except psutil.NoSuchProcess:
                               	pass
               	return Response(process_list, status=status.HTTP_200_OK)
	
	def post(self, request, format=None):
		data_req= request.data
		msg={}

		if (data_req.keys()[0] == 'command'):
			command= data_req['command']
                        args = shlex.split(command)		
			try:
				process_pid= subprocess.Popen(args, stdout=subprocess.PIPE).pid
                                p = psutil.Process(process_pid)
                                #una vez lanzado el proceso
				while True:
                                        if (p.status() != 'zombie' or p.status() != 'sleeping'):
                                                break 
				child = p.get_children()
                                if len(child) > 0:
                                        process_pid = child[0].pid			
				msg['status']= "proceso creado"
				msg['pid']= process_pid		
				return Response(msg, status=status.HTTP_201_CREATED)
			except:
				msg = {'status': 'proceso no ejecutado con el comando ingresado'}
				return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
		else:
			msg = {'status': 'no se encontro una orden llamada command'}
			return Response(msg, status=status.HTTP_400_BAD_REQUEST)

class ProcessView(APIView):
	
	authentication_classes= (TokenAuthentication, )
	permission_classes= (IsAuthenticated, )
	
	def get_permissions(self):
        	# Your logic should be all here
        	if self.request.method == 'GET':
        		self.permission_classes = [IsAuthenticated, ]
        	else:
           		self.permission_classes = [IsAdminUser, ]

        	return super(ProcessView, self).get_permissions()


	def get(self, request, pk, format=None):
		
		#validamos que exista el pid y si no enviamos un 404.
		pk= int(pk)
    		pid_exist= psutil.pid_exists(pk)
		if not pid_exist:
			msg= {'status': 'no se encontro un proceso con ese pid'}
        		return Response(msg, status=status.HTTP_404_NOT_FOUND)
		#hacer algo con el proceso seleccionado
	        proc= psutil.Process(pk)
        	pinfo= proc.as_dict(['username', 'pid', 'name', 'cpu_percent', 'memory_percent', 'memory_info', 'terminal', 'status', 'cpu_times', 'cmdline'])
        	pinfo['priority']= proc.nice()
        	create_time= datetime.datetime.fromtimestamp(proc.create_time()).strftime("%Y-%m-%d %H:%M:%S")
        	pinfo['create_time']= create_time
        	return Response(pinfo, status=status.HTTP_200_OK)	
	def put(self, request, pk, format=None):

		pk= int(pk)
		pid_exits= psutil.pid_exists(pk)
		if not pid_exits:
			msg= {'status': 'no se encontro un proceso con ese pid'}
			return Response(msg, status=status.HTTP_404_NOT_FOUND)
		param= request.data
		
		if (param.keys()[0] == "priority"):
			priority= int(param['priority'])
			if (-20<priority<20):
				try:
					proc= psutil.Process(pk)
					proc.nice(priority)
					msg= {'status': 'prioridad asignada'}
					return Response(msg, status=status.HTTP_200_OK)
				except psutil.AccessDenied:
					msg= {'status': 'imposible cambiar prioridad, no tiene permisos'}
					return Response(status=status.HTTP_401_UNAUTHORIZED)
			else:
				msg = {'status': 'prioridad fuera de rango, recuerde entre -20 y 20'}
				return Response(msg, status=status.HTTP_400_BAD_REQUEST)
			
		else:
			msg = {'status': 'no se encontro una prioridad, recuerde -d priority=nro'}	
			return Response(msg, status=status.HTTP_400_BAD_REQUEST)

	def delete(self, request, pk, format=None):
             	#validamos que exista el pid y si no enviamos un 404.
                pk= int(pk)
                pid_exist= psutil.pid_exists(pk)
                if not pid_exist:
			msg= {'status': 'no se encontro un proceso con ese pid'}
			return Response(msg, status=status.HTTP_404_NOT_FOUND)                

		#buscamos ssh, <<No nos desinflemos la bicicleta entre mormones>>
		grep= sh.grep(sh.ps("cax"), 'ssh')
		list_pid_ssh= []
		grep_row= grep.split('\n')
		cant= len(grep_row) -1
        	#quitamos el ultimo vacio
        	aux= grep_row[0:cant]

        	for row in aux:
                	ap= row.split( )
                	list_pid_ssh.append(ap[0])
        	#si el pid pasado por el usuario es igual al del ssh enviamos operacion no aceptable
        	for pid_ssh in list_pid_ssh:
                	if pid_ssh == pk:
				msg= {'status': 'proceso ssh no se puede detener'}
                        	return Response(msg, status=status.HTTP_406_NOT_ACCEPTABLE)
        	proc= psutil.Process(pk)
		try:
			proc.kill()
			#proceso nro pid pasado por argumento fue eliminado
			msg= {'status': 'proceso con el pid eliminado'}
			return Response(msg, status=status.HTTP_200_OK)
		except AcessDenied:
			#error en permisos al matar ese proceso
			msg= {'status': 'no posee permisos para eliminar el proceso con el pid ingresado'}
			return Response(msg, status=status_406_NOT_ACCEPTABLE)

