from django.conf.urls import patterns, url
from rest_framework.authtoken import views
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

"""
urlpatterns = patterns(
    'api.views',
    url(r'^process/$', 'process_list', name='process_list'),
    url(r'^process/(?P<pk>[0-9]+)$', 'process_detail', name='process_detail'),
    url(r'^api-token-auth/', views.obtain_auth_token),
)
"""


urlpatterns = [
    url(r'^process/$', views.ProcessList.as_view()),
    url(r'^process/(?P<pk>[0-9]+)/$', views.ProcessView.as_view()),
    url(r'^token/', 'rest_framework.authtoken.views.obtain_auth_token'),
 #url(r'^snippets/(?P<pk>[0-9]+)/$', views.SnippetDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)

